//摇一摇部分
// “摇一摇”的动作即“一定时间内设备移动了一定的距离”，
// 因此通过监听上一步获取到的x, y, z 值在一定时间范围内的变化率，即可进行设备是否有进行晃动的判断。
// 而为了防止正常移动的误判，需要给该变化率设置一个合适的临界值。
var SHAKE_THRESHOLD = 1000
// 速度临界点值，定义一个摇动的阀值
var last_update = 0
// 定义一个变量保存上次更新的时间（毫秒）
var last_time = 0
// 上一次
var x
var y
var z
var last_x
var last_y
var last_z
// 定义x、y、z记录三个轴的数据以及上一次出发的时间

var sound = new Howl({
    src: ['./audio/yao.wav', './audio/1.mp3'],
    format: ['mp3', 'wav'],
    onload: function(){
        sound.play()
    }
})
var findsound = new Howl({
    src: ['./audio/ok.wav', './audio/2.mp3'],
    format: ['mp3', 'wav'],
    onload: function(){
        findsound.play()
    }
})

var curTime
// var isShakeble = true

// deviceOrientation：封装了方向传感器数据的事件，可以获取手机静止状态下的方向数据，例如手机所处角度、方位、朝向等
// deviceMotion：封装了运动传感器数据的事件，可以获取手机运动状态下的运动加速度等数据，
//               使用它我们能够很容易的实现重力感应、指南针等有趣的功能，在手机上将非常有用
// DeviceMotionEvent(设备运动事件)返回设备有关于加速度和旋转的相关信息，
//                  加速度的数据将包含三个轴：x，y和z
//                  (x轴横向贯穿手机屏幕或者笔记本键盘，y轴纵向贯穿手机屏幕或笔记本键盘，z轴垂直于手机屏幕或笔记本键盘)
//                  因为有些设备可能没有硬件来排除重力的影响，该事件会返回两个属性，accelerationIncludingGravity(含重力的加速度)和acceleration(加速度)，后者排除了重力的影响
function init() {
    // 监听运动传感事件
    if (window.DeviceMotionEvent) {
        window.addEventListener('devicemotion', deviceMotionHandler, false)
        // addEventListener()
        // 第一个参数是需要监听的事件
        // 第二个参数是事件发生时执行的函数的名字
        // 第三个参数false表示事件在冒泡阶段发生，true表示在挖洞结段发生
    } else {
        alert('您的手机不支持摇一摇')
    }
}

function deviceMotionHandler(eventData) {
    curTime = new Date().getTime()
    // getTime() 获取时间的毫秒数
    var diffTime = curTime - last_update
    // 计算当前时间与上次更新时间的时间差值（毫秒数），100毫秒进行一次位置判断
    if (diffTime > 100) {
        var acceleration = eventData.accelerationIncludingGravity
        // 获取含重力的加速度
        last_update = curTime
        x = acceleration.x
        // x 轴方向上的加速度（排除了重力影响）
        y = acceleration.y
        z = acceleration.z
        var speed = Math.abs(x + y + z - last_x - last_y - last_z) / diffTime * 10000
        // Math.abs() 取绝对值
        // 100毫秒进行一次位置判断，若前后x, y, z间的差值的绝对值和时间比率超过了预设的阈值，则判断设备进行了摇晃操作。
        // $('<p></p>').text('x:'+x+"<br>y:"+y+"<br>z:"+z).appendTo('body')
        // 可以在此进行输出，在手机上可看到数据变化
        if (speed > SHAKE_THRESHOLD && $("#loading").attr('class') == "loading") {
            shake()
        }
        last_x = x
        last_y = y
        last_z = z
    }
}
var count = 0
function shake() {
    last_time = curTime
    $("#loading").attr('class', 'loading loading-show')
    $("#shakeup").animate({ top: "10%" }, 700, function () {
        findsound.play()
        $("#shakeup").animate({ top: "25%" }, 700, function () {
            $("#loading").attr('class', 'loading')
            // count++
            // alert("哥们，摇了" + count + "次了")
            window.location.href="https://hjm100.gitee.io/lovetanabata/loveLetter/Applove/love.html"
        })
    })
    $("#shakedown").animate({ top: "40%" }, 700, function () {
        $("#shakedown").animate({ top: "25%" }, 700, function () {})
    })
    sound.play()
}
Howler.mobileAutoEnable = true
//初始化
$(document).ready(function () {
    
    // 默认情况下，音频在iOS系统中被锁定到一个声音在用户相互作用，并发挥正常的网页会话的其余部分
    // howler.js的默认行为是企图默默地打开音频播放的第一个touchend事件
    // 打一个空缓冲区
    // 这种行为可以被调用
    // 详细参考：http://www.uedsc.com/howler-js.html
    init()
})
