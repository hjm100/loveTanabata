var texts = [
    '梦梦大人',
    '想你想你么么哒',
    '还记得第一次牵手的时间吗',
    '2014年12月24日',
    '我迈出了人生最重要的一步',
    '精心准备的flash动画',
    '精心挑选的苹果',
    '精心为你点的歌谣',
    'take me to your heart',
    '响遍工程学院的每个角落',
    '胖嘟嘟的小龙猫',
    '萌萌哒的小黄人',
    '带着愿望的孔明灯',
    '照亮甜甜的小西门',
    '平安夜的表白',
    '希望梦梦大人永远平安',
    '不辜负梦梦大人的考验',
    '往后余生都是你',
    '回忆总是那么浪漫',
    '命中注定与你相遇',
    '不迟到的短信',
    '开启扰民计划',
    '细心的编写',
    '塑造文字长征',
    '图书馆中傻傻的等待',
    '毛概论文无休止的修改',
    '明月湖畔的窃窃私语',
    '清明开封初感生活',
    '洛阳相识闺蜜团',
    '新郑出游始祖山',
    '绿博园开心闯关',
    '惠州海边划心心',
    '再游开封的攻略',
    '包公祠到老河大',
    '清明上河看演出',
    '暑假鸿基梦家政',
    '生活处处是惊喜',
    '陪梦梦大人开心',
    '陪梦梦大人难过',
    '实习的酸甜苦辣',
    '培训的尽心尽力',
    '梦梦大人的理解',
    '梦梦大人的体谅',
    '你就是我的小幸运',
    '16年是的七夕',
    '带着惊喜和感动',
    '宝宝的专属网页',
    '宝宝的专属祝福',
    '每一张便利贴',
    '都是一个幸福',
    '北京我们小天地',
    '长城鸟巢圆明园',
    '北京清华天安门',
    '故宫天坛颐和园',
    '八角香山欢乐谷',
    '旋转木马转圈圈',
    '激流勇进过山车',
    '丛林飞车奥德赛',
    '飞跃牛奶碰碰车',
    '回忆的笔记',
    '此生有梦梦',
    '幸福的年轮',
    '尽在鸿基梦',
    '你是我眼中最美的风景',
    '你不在北京',
    '七夕尽是回忆',
    '梦梦大人',
    '今夜有梦梦',
    '不愿独孤眠',
    '异地相思苦',
    '但愿是尽头',
    '梦梦大人',
    '前方高能',
    '七夕作品集',
    '带你回味',
    '带你感受进步',
    '2014年08月02日',
    '星期六 (甲午年（马年）七月初七',
    '让我来不及准备',
    '爱需要用一生去补偿',
    '2015年08月20日',
    '星期四 (乙未年（羊年）七月初七',
    '爱的执着(不迟到的短信)',
    '25124多个字',
    '爱的万里长征',
    '2016年08月09日',
    '星期二 (丙申年（猴年）七月初七',
    '爱的记忆(甜蜜相册)',
    '回忆的老照片',
    '音乐相册满满爱',
    '2017年08月28日',
    '星期一 (丁酉年（鸡年）七月初七',
    '爱的神秘(神秘的电报)',
    '回忆相遇的爱情',
    '把你放在心里',
    '2018年08月17日',
    '星期五 (戊戌年（狗年）七月初七',
    '爱的深情(说不完的土味情话)',
    '迟到的枕边语',
    '送个你说不完的土味情话',
    '2019年08月07日',
    '星期三 (己亥年（猪年）七月初七',
    '爱的思念(陪你去看流星雨)',
    '流星雨中下许愿',
    '同一星空下的距离',
    '2020年08月25日',
    '星期二 (庚子年（鼠年）七月初七',
    '爱的果实(我想陪你一起过)',
    '往后余生',
    '风雪是你',
    '平淡是你',
    '我的世界都是你'
]